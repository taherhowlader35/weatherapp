
//
//  WeatherModel.swift
//  WeatherApp
//
//  Created by Taher Howlader on 11/2/22.
//

import Foundation

// MARK: - Welcome
struct WeatherModel: Codable {
    let coord: Coord
    let weather: [Weather]
    let main: Main
    let name: String
    let sys: Sys
}

// MARK: - Coord
struct Coord: Codable {
    let lon, lat: Double
}

// MARK: - Weather
struct Weather: Codable {
    let icon: String
    enum CodingKeys: String, CodingKey {
        case icon
    }
}
// MARK: - Main
struct Main: Codable {
    let temp: Double
    
    enum CodingKeys: String, CodingKey {
        case temp
    }
}

// MARK: - Sys
struct Sys: Codable {
    let country: String
}



